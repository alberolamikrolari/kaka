library ieee;
use ieee.std_logic_1164.all;


entity simulation is
end simulation;

architecture behavior of simulation is

    -- component declaration for the unit under test (uut)

    component main
    port(
         clk : in  std_logic;
         reset : in  std_logic;
         led : out  std_logic
        );
    end component;


   --inputs
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';

  --outputs
   signal led : std_logic;

   -- clock period definitions
   constant clk_period : time := 20 ns;   -- 50mhz

begin

  -- instantiate the unit under test (uut)
   uut: main port map (
          clk => clk,
          reset => reset,
          led => led
        );

   -- clock process definitions
   clk_process :process
   begin
    clk <= '0';
    wait for clk_period/2;
    clk <= '1';
    wait for clk_period/2;
   end process;


   -- stimulus process
   stim_proc: process
   begin

    reset <= '0';

    -- hold reset state for 100 ns.
      wait for 100 ns;

    reset <= '1';

      wait for clk_period*10;

      -- insert stimulus here

      wait;
   end process;

end;
