library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;


entity main is
    port ( clk : in  std_logic;
           reset : in  std_logic;
           led : out  std_logic);
end main;

architecture behavioral of main is

	signal cnt_led : std_logic_vector(31 downto 0);
	constant c_end : std_logic_vector(31 downto 0) := x"006940aa";
	signal led_i : std_logic;
begin


	p_cnt : process(clk, reset)
	begin
		if(reset = '0') then
			cnt_led <= (others => '0');
		elsif(clk'event and clk = '1') then
			if(cnt_led < c_end) then
				cnt_led <= cnt_led + 1;
			else
				cnt_led <= (others => '0');
			end if;
		end if;
	end process;

	p_led : process(clk, reset)
	begin
		if(reset = '0') then
			led_i <= '0';
		elsif(clk'event and clk = '1') then
			if(cnt_led = c_end) then
				led_i <= not(led_i);
			end if;
		end if;
	end process;

	led <= led_i;

end behavioral;
